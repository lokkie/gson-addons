package org.lokkie.gson.adapters

import com.google.gson.* // ktlint-disable
import org.lokkie.types.PartialMappedObject
import org.lokkie.types.any.getTypeOfField
import org.lokkie.types.any.isFieldExists
import org.lokkie.types.any.setAnyField
import org.lokkie.types.field.serializedName
import org.lokkie.utils.allFields
import java.lang.reflect.Type

/**
 * Adapter for serialize/deserialize object implemented PartialMappedObject
 *
 * @author lokkie
 * @version 0.1
 */
class PartialJsonMapper : JsonDeserializer<PartialMappedObject>, JsonSerializer<PartialMappedObject> {

    /**
     * Gson invokes this call-back method during deserialization when it encounters a field of the
     * specified type.
     * <p>In the implementation of this call-back method, you should consider invoking
     * {@link JsonDeserializationContext#deserialize(JsonElement, Type)} method to create objects
     * for any non-trivial field of the returned object. However, you should never invoke it on the
     * the same type passing {@code json} since that will cause an infinite loop (Gson will call your
     * call-back method again).
     *
     * @param json The Json data being deserialized
     * @param typeOfT The type of the Object to deserialize to
     * @return a deserialized object of the specified type typeOfT which is a subclass of {@code T}
     * @throws JsonParseException if json is not in the expected format of {@code typeofT}
     */
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): PartialMappedObject? {
        if (json.isJsonNull) {
            return null
        }
        return (Class.forName(typeOfT.typeName).newInstance() as PartialMappedObject).apply {
            val jObj: JsonObject = json.asJsonObject
            jObj.entrySet().forEach {
                if (isFieldExists(it.key)) {
                    setAnyField(it.key, context.deserialize(it.value, getTypeOfField(it.key)) as Any?)
                } else {
                    this[it.key] = context.deserialize(it.value, Any::class.java)
                }
            }
        }
    }

    /**
     * Gson invokes this call-back method during serialization when it encounters a field of the
     * specified type.
     *
     * <p>In the implementation of this call-back method, you should consider invoking
     * {@link JsonSerializationContext#serialize(Object, Type)} method to create JsonElements for any
     * non-trivial field of the {@code src} object. However, you should never invoke it on the
     * {@code src} object itself since that will cause an infinite loop (Gson will call your
     * call-back method again).</p>
     *
     * @param src the object that needs to be converted to Json.
     * @param typeOfSrc the actual type (fully genericized version) of the source object.
     * @return a JsonElement corresponding to the specified object.
     */
    override fun serialize(src: PartialMappedObject?, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        if (src == null) return JsonNull.INSTANCE
        val result = JsonObject()
        Class.forName(typeOfSrc.typeName).allFields.filterNot { it.name == "mapForUnknownFields" }.forEach {
            it.isAccessible = true
            result.add(it.serializedName, context.serialize(it.get(src)))
        }
        src.mapForUnknownFields.forEach { name, value -> result.add(name, context.serialize(value)) }
        return result
    }
}