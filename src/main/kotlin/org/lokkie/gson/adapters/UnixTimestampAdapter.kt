package org.lokkie.gson.adapters

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import org.lokkie.types.UnixTimestamp
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.regex.Pattern

/**
 * GSon Type adapter for UnixTimestamp class
 */
class UnixTimestampAdapter : TypeAdapter<UnixTimestamp>() {
    private val patterns = arrayOf(
            "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd'T'HH:mm:ssZ",
            "MM/dd/yyyy'T'HH:mm:ssZ", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "MM/dd/yyyy'T'HH:mm:ss.SSS'Z'",
            "MM/dd/yyyy'T'HH:mm:ss.SSSZ", "MM/dd/yyyy'T'HH:mm:ss.SSS",
            "yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy HH:mm:ss",
            "MM/dd/yyyy'T'HH:mm:ss", "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy:MM:dd HH:mm:ss", "yyyy-MM-dd", "yyyyMMdd"
    )

    /**
     * Writes one JSON value (an array, object, string, number, boolean or null)
     * for {@code value}.
     *
     * @param value the Java object to write. May be null.
     */
    @Throws(IOException::class)
    override fun write(out: JsonWriter, value: UnixTimestamp?) {
        if (value == null) {
            out.nullValue()
        } else {
            out.value(value.long)
        }
    }

    /**
     * Reads one JSON value (an array, object, string, number, boolean or null)
     * and converts it to a Java object. Returns the converted object.
     *
     * @return the converted Java object. May be null.
     */
    @Throws(IOException::class)
    override fun read(`in`: JsonReader): UnixTimestamp? {
        if (`in`.peek() == JsonToken.NULL) {
            `in`.nextNull()
            return null
        }
        val jsonData = `in`.nextString()
        var result: UnixTimestamp? = null
        if (Pattern.compile("^\\d+$").matcher(jsonData).matches()) {
            result = UnixTimestamp(jsonData)
        } else {
            for (format in patterns) {
                try {
                    result = UnixTimestamp(SimpleDateFormat(format, Locale.ENGLISH).parse(jsonData))
                    break
                } catch (ignored: Exception) {}
            }
        }
        return result
    }
}