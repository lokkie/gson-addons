package org.lokkie.types.field

import com.google.gson.annotations.SerializedName
import java.lang.reflect.Field

/**
 * Gets value of annotation SerializedName if it is set for field
 */
val Field.serializedName: String
    get() = if (isAnnotationPresent(SerializedName::class.java))
        getAnnotation(SerializedName::class.java).value
    else name