package org.lokkie.types

/**
 * Interface for object partial mapping. Allows safe unmapped fields and serialize them together with maooed
 */
interface PartialMappedObject {
    /**
     * Storage for unmapped fields
     */
    val mapForUnknownFields: LinkedHashMap<String, Any?>

    /**
     * Allows to get unmapped field by nmae
     *
     * @param name name o search field
     */
    operator fun get(name: String): Any? = mapForUnknownFields[name]

    /**
     * Allows to set unmapped field by name
     *
     * @param name name of field to set
     * @param value value to set into unmapped field
     */
    operator fun set(name: String, value: Any?) {
        mapForUnknownFields[name] = value
    }
}