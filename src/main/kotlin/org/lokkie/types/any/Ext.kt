package org.lokkie.types.any

import com.google.gson.annotations.SerializedName
import org.lokkie.utils.allFields

/**
 * Checks if specified field exists in object
 *
 * @param name field name to ckeck
 */
fun Any?.isFieldExists(name: String): Boolean = this?.javaClass?.allFields?.any {
    (it.isAnnotationPresent(SerializedName::class.java) && it.getAnnotation(SerializedName::class.java).value == name)
            || it.name == name
} ?: false

/**
 * Sets value to field using reflection
 *
 * @param name field name to set
 * @param value value to set
 */
fun Any?.setAnyField(name: String, value: Any?) = this?.getFieldByNameOrSerializedName(name)?.also {
    it.isAccessible = true
    if (value == null) {
        try {
            it.set(this, null)
        } catch (ignored: Exception) {
        }
    } else {
        if (it.get(this)?.javaClass ?: it.type == value.javaClass) {
            it.set(this, value)
        }
    }
}

/**
 * Gets field real type
 *
 * @param name field name to gat type of
 */
fun Any?.getTypeOfField(name: String) = this?.getFieldByNameOrSerializedName(name)?.let {
    it.isAccessible = true
    it.get(this)?.javaClass ?: it.type
}

/**
 * Gets field by name or annotation SerializedName value
 *
 * @param name name to search by
 */
fun Any?.getFieldByNameOrSerializedName(name: String) = this?.javaClass?.allFields?.firstOrNull {
    (it.isAnnotationPresent(SerializedName::class.java) && it.getAnnotation(SerializedName::class.java).value == name)
            || it.name == name
}